<?php
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
require_once( dirname( dirname( dirname( __FILE__ ))) . '/load.php' );
function shutdown(){
	file_put_contents("serverStatus.txt", "0");
	require_once "startServer.php";
}
register_shutdown_function('shutdown');
if( isset($startNow) ){
	require_once "vendor/autoload.php";
	require_once "class.chat.php";
	$server = IoServer::factory(
		new ChatServer(),
        8081
	);
    $server->run();
}
?>